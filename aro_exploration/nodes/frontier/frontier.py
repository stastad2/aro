#!/usr/bin/env python3

import numpy as np
import rospy
from scipy.ndimage import morphology
from nav_msgs.msg import OccupancyGrid, MapMetaData
from visualization_msgs.msg import Marker, MarkerArray
from geometry_msgs.msg import Pose2D
import tf2_ros
from aro_msgs.srv import GenerateFrontier, GenerateFrontierRequest, GenerateFrontierResponse
from aro_msgs.srv import PlanPath, PlanPathRequest, PlanPathResponse
from typing import Optional



invalid_pose = Pose2D(np.nan, np.nan, np.nan)


def get_grid_position(pose2d: Pose2D, grid_info: MapMetaData) -> np.ndarray:
    pos = np.array([0, 0, 0, 1])
    # TODO convert the pose to grid position

    return pos


def grid_to_map_coordinates(position: np.ndarray, grid_info: MapMetaData) -> np.ndarray:
    # position is (2,) numpy array
    # TODO convert the grid position to map pose
    pos = np.array([0, 0])

    return pos


class FrontierExplorer:
    def __init__(self):
        self.map_frame = rospy.get_param("~map_frame", "icp_map")
        self.robot_frame = rospy.get_param("~robot_frame", "base_footprint")
        self.robot_diameter = float(rospy.get_param("~robot_diameter", 0.8))
        self.min_frontier_size = rospy.get_param("~min_frontier_size", 3)
        self.occupancy_threshold = int(rospy.get_param("~occupancy_threshold", 90))

        # Helper variable to determine if grid was received at least once
        self.grid_ready = False
        self.stale_wfd = True  # True if WFD was not recomputed for the newest grid

        # You may wish to listen to the transformations of the robot
        self.tf_buffer = tf2_ros.Buffer()
        self.tf_listener = tf2_ros.TransformListener(self.tf_buffer)

        # Path planner can be utilized for testing reachability
        rospy.wait_for_service('plan_path')
        self.plan_path = rospy.ServiceProxy('plan_path', PlanPath)

        self.vis_pub = rospy.Publisher('frontier_vis', MarkerArray, queue_size=2)
        self.vis_map_pub = rospy.Publisher('frontier_map', OccupancyGrid, queue_size=2)

        # The services and other variables are initialized once an occupancy grid has been received
        self.grf_service: Optional[rospy.ServiceProxy] = None
        self.gcf_service: Optional[rospy.ServiceProxy] = None
        self.gbv_service: Optional[rospy.ServiceProxy] = None
        self.grg_service: Optional[rospy.ServiceProxy] = None

        # TODO: you may wish to do initialization of other variables
        self.wfd = None  # TODO: implement WFD computation helper
        self.robot_position: Optional[np.ndarray] = None
        self.robot_vis_position: Optional[np.ndarray] = None
        self.resolution: Optional[float] = None
        self.occupancy_grid: Optional[np.ndarray] = None
        self.visibility_grid: Optional[np.ndarray] = None
        self.origin_pos: Optional[np.ndarray] = None
        self.vis_origin_pos: Optional[np.ndarray] = None
        self.grid_info: Optional[MapMetaData] = None
        self.vis_grid_info: Optional[MapMetaData] = None

        # Subscribe to grids
        self.grid_subscriber = rospy.Subscriber('occupancy', OccupancyGrid, self.grid_cb)
        self.vis_grid_subscriber = rospy.Subscriber('visible_occupancy', OccupancyGrid, self.vis_grid_cb)

    def compute_wfd(self):
        """ Run the Wavefront detector """
        rospy.loginfo('Finding frontiers')
        frontiers = []

        # TODO:
        # TODO: First, you should try to obtain the robots coordinates

        # TODO: Then, copy the occupancy grid into some temporary variable and inflate the obstacles

        # TODO: Run the WFD algorithm - see the presentation slides for details on how to implement it



        self.publish_frontiers_vis(frontiers)
        if len(frontiers) == 0:
            rospy.logwarn("No frontier found.")
        return frontiers

    def publish_frontiers_vis(self, frontiers: np.ndarray):
        marker_array = MarkerArray()
        marker = Marker()
        marker.action = marker.DELETEALL
        marker.header.frame_id = self.map_frame
        marker_array.markers.append(marker)
        self.vis_pub.publish(marker_array)

        marker_array = MarkerArray()
        marker_id = 0
        for f in frontiers:
            fx = [f.middle()[0] for f in frontiers]
            fy = [f.middle()[1] for f in frontiers]
            for i in range(len(f.points)):
                marker = Marker()
                marker.ns = 'frontier'
                marker.id = marker_id
                marker_id += 1
                marker.header.frame_id = self.map_frame
                marker.type = Marker.CUBE
                marker.action = 0
                marker.scale.x = self.resolution
                marker.scale.y = self.resolution
                marker.scale.z = self.resolution
                x, y = grid_to_map_coordinates(np.array([f.points[i][1], f.points[i][0]]), self.grid_info)
                marker.pose.position.x = x
                marker.pose.position.y = y
                marker.pose.position.z = 0
                marker.pose.orientation.x = 0
                marker.pose.orientation.y = 0
                marker.pose.orientation.z = 0
                marker.pose.orientation.w = 1
                marker.color.r = 0.0
                marker.color.g = 1.0
                marker.color.b = 0.0
                marker.color.a = 1.0
                marker_array.markers.append(marker)
        self.vis_pub.publish(marker_array)

    def get_random_frontier(self, request: GenerateFrontierRequest) -> GenerateFrontierResponse:
        """ Return random frontier """
        if self.stale_wfd:
            self.compute_wfd()
        frontiers = self.wfd.frontiers
        if len(frontiers) == 0:
            return GenerateFrontierResponse(invalid_pose)
        frontier = np.random.choice(frontiers)
        
        frontier_center = frontier.middle()
        x, y = grid_to_map_coordinates(np.array([frontier_center[0], frontier_center[1]]), self.grid_info)

        response = GenerateFrontierResponse(Pose2D(x, y, 0.0))
        rospy.loginfo('Returning random frontier at ' + str(x) + ' ' + str(y))
        return response

    def get_closest_frontier(self, request: GenerateFrontierRequest) -> GenerateFrontierResponse:
        """ Return frontier closest to the robot """
        rospy.loginfo('Finding closest frontier.')
        if self.stale_wfd:
            self.compute_wfd()
        frontiers = self.wfd.frontiers
        if len(frontiers) == 0:
            return GenerateFrontierResponse(invalid_pose)

        best_frontier_idx = 0  # TODO: compute the index of the closest frontier

        frontier = frontiers[best_frontier_idx]

        frontier_center = frontier.middle()
        x, y = grid_to_map_coordinates(np.array([frontier_center[1], frontier_center[0]]), self.grid_info)
        response = GenerateFrontierResponse(Pose2D(x, y, 0.0))
        rospy.loginfo('Returning closest frontier at ' + str(x) + ' ' + str(y))
        return response

    def get_best_value_frontier(self, request: GenerateFrontierRequest) -> GenerateFrontierResponse:
        """ Return largest frontier """
        rospy.loginfo('Finding best value frontier.')
        if self.stale_wfd:
            self.compute_wfd()
        frontiers = self.wfd.frontiers
        if len(frontiers) == 0:
            return GenerateFrontierResponse(invalid_pose)

        best_frontier_idx = 0  # TODO: compute the index of the best frontier

        frontier = frontiers[best_frontier_idx]


        frontier_center = frontier.middle()
        x, y = grid_to_map_coordinates(np.array([frontier_center[1], frontier_center[0]]), self.grid_info)

        response = GenerateFrontierResponse(Pose2D(x, y, 0.0))
        rospy.loginfo('Returning best frontier at '+str(x)+' '+str(y))
        return response

    def get_random_goal(self, request: GenerateFrontierRequest) -> GenerateFrontierResponse:
        """ Return random empty cell in the grid """
        rospy.loginfo('Finding random goal position.')
        self.get_robot_coordinates()
        robot_position = np.array([self.robot_position[1], self.robot_position[0]])
        goal_position = np.array([0, 0])
        # TODO: get the coordinates of a random empty cell



        x, y = grid_to_map_coordinates(np.array([goal_position[1], goal_position[0]]), self.grid_info)
        response = GenerateFrontierResponse(Pose2D(x=x, y=y, theta=0.0))
        rospy.loginfo('Returning random position at .'+str(x)+' '+str(y))
        return response

    def get_robot_coordinates(self):
        """ Get the current robot position in the grid """
        try:
            trans = self.tf_buffer.lookup_transform(self.map_frame, self.robot_frame, rospy.Time(), rospy.Duration(0.5))
        except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException):
            rospy.logwarn("Cannot get the robot position!")
            self.robot_position = None
            self.robot_vis_position = None
        else:
            self.robot_position = get_grid_position(trans.transform.translation, self.grid_info).astype(int)
            self.robot_vis_position = \
                get_grid_position(trans.transform.translation, self.vis_grid_info).astype(int)

    def extract_grid(self, msg):
        width, height, self.resolution = msg.info.width, msg.info.height, msg.info.resolution
        self.origin_pos = np.array([msg.info.origin.position.x, msg.info.origin.position.y])
        self.grid_info = msg.info
        self.occupancy_grid = np.reshape(msg.data, (height, width))

    def extract_vis_grid(self, msg):
        width, height = msg.info.width, msg.info.height
        self.vis_origin_pos = np.array([msg.info.origin.position.x, msg.info.origin.position.y])
        self.vis_grid_info = msg.info
        self.visibility_grid = np.reshape(msg.data, (height, width))

    def grid_cb(self, msg):
        self.extract_grid(msg)
        self.stale_wfd = True
        if not self.grid_ready:
            # TODO: Do some initialization of necessary variables
            self.wfd = None


            # Create services
            self.grf_service = rospy.Service('get_random_frontier', GenerateFrontier, self.get_random_frontier)
            self.gcf_service = rospy.Service('get_closest_frontier', GenerateFrontier, self.get_closest_frontier)
            self.gbv_service = rospy.Service('get_best_value_frontier', GenerateFrontier, self.get_best_value_frontier)
            self.grg_service = rospy.Service('get_random_goal_pose', GenerateFrontier, self.get_random_goal)
            self.grid_ready = True

    def vis_grid_cb(self, msg):
        self.extract_vis_grid(msg)


if __name__ == "__main__":
    rospy.init_node("frontier_explorer")
    fe = FrontierExplorer()
    rospy.spin()
