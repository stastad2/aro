#!/usr/bin/env python3
"""
Simple reactive controller for turtlebot robot.
"""

import rospy
import numpy as np  # you probably gonna need this
from geometry_msgs.msg import Twist, Vector3
from aro_msgs.msg import SectorDistances
from std_srvs.srv import SetBool, SetBoolRequest, SetBoolResponse, Trigger


def callback(data):
    rospy.loginfo(rospy.get_caller_id() + "I heard %s", data.data)


# TODO HW 01 and HW 02: add necessary imports
from sensor_msgs.msg import LaserScan


class ReactiveController():
    TimerRunning = False
    StopRobot = True

    def __init__(self):
        rospy.loginfo('Initializing node')
        rospy.init_node('reactive_controller')
        self.initialized = False

        # TODO HW 01: register listener for laser scan message
        self.subscriberLaser = rospy.Subscriber('scan', LaserScan, self.scan_cb)

        # TODO HW 02:publisher for "/cmd_vel" topic (use arg "latch=True")
        self.publisherTwist = rospy.Publisher('cmd_vel', Twist, latch=True, queue_size=1)

        # TODO HW 01: register publisher for "/reactive_control/sector_dists" topic
        self.publisherSector = rospy.Publisher('/reactive_control/sector_dists', SectorDistances, queue_size=1,
                                               latch=True)

        # TODO HW 02: create proxy for the mission evaluation service and wait till the start service is up
        rospy.wait_for_service('/reactive_control/evaluate_mission')
        self.Proxy = rospy.ServiceProxy('/reactive_control/evaluate_mission', Trigger)

        # TODO HW 02: create service server for mission start
        self.service = rospy.Service('/reactive_control/activate', SetBool, self.activate_cb)
        rospy.wait_for_service('/reactive_control/activate')
        # TODO: you are probably going to need to add some variables

        # TODO HW 02: add timer for mission end checking

        if not self.TimerRunning:
            self.timer = rospy.Timer(rospy.Duration(50), self.timer_cb, oneshot=True)
            self.TimerRunning = True
            rospy.loginfo('Timer running, robot running')

        self.initialized = True

        rospy.loginfo('Reactive controller initialized. Waiting for data.')

    def timer_cb(self, event):
        """
        Callback function for timer.

        :param event (rospy TimerEvent): event handled by the callback
        """

        if not self.initialized:
            return

        # TODO HW 02: Check that the active time had elapsed and send the mission evaluation request
        self.TimerRunning = False
        self.StopRobot = True
        self.Proxy()

    def activate_cb(self, req: SetBoolRequest) -> SetBoolResponse:
        """
        Activation service callback.

        :param req: obtained ROS service request

        :return: ROS service response
        """

        # TODO HW 02: Implement callback for activation service
        if req.data:
            self.StopRobot = False
        else:
            self.StopRobot = True
            self.apply_control_inputs(0, 0)
            rospy.loginfo('Robot stopped')

        rospy.loginfo_once('Activation callback entered')
        Response = SetBoolResponse()
        Response.success = True
        if self.StopRobot:
            Response.message = "Robot stopped"
        else:
            Response.message = "Robot running"
        return Response

    def scan_cb(self, scan: LaserScan):
        """
        Scan callback.

        :param msg: obtained message with data from 2D scanner of type ???
        """
        rospy.loginfo_once('Scan callback entered')

        # TODO HW 01: Implement callback for 2D scan, process and publish required data
        def index(angle: float):
            return int(np.ceil((np.deg2rad(angle) - float(scan.angle_min)) / float(scan.angle_increment)))

        data = np.array(scan.ranges)
        data[data < scan.range_min] = None
        data[data > scan.range_max] = None

        message = SectorDistances()
        message.distance_left = np.min(data[index(30):index(90)])
        message.distance_right = np.min(data[index(270):index(330)])
        message.distance_front = np.min(np.concatenate((data[index(330):], data[:index(30)])))
        self.publisherSector.publish(message)

        # TODO HW 02: Add robot control based on received scan
        DistSum = message.distance_left + message.distance_right
        if not self.StopRobot:
            if message.distance_left > message.distance_right:
                angle = 1
            else:
                angle = -1
            if message.distance_left < message.distance_right:
                rate = -1 + message.distance_left / DistSum
            else:
                rate = 1 - message.distance_right / DistSum

            if message.distance_front >= 1:
                self.apply_control_inputs(0.75, rate)
            elif message.distance_front >= 0.5:
                self.apply_control_inputs(0.3, rate)
            elif message.distance_front >= 0.2:
                self.apply_control_inputs(0.2, rate)
            else:
                self.apply_control_inputs(0, angle * 0.5)
        else:
            self.apply_control_inputs(0, 0)

        if not self.initialized:
            return

    def apply_control_inputs(self, velocity: float, angular_rate: float):
        """
        Applies control inputs.

        :param velocity: required forward velocity of the robot
        :param angular_rate: required angular rate of the robot
        """
        # TODO HW 02: publish required control inputs

        self.publisherTwist.publish(Twist(Vector3(velocity, 0, 0), Vector3(0, 0, angular_rate)))

        if not self.initialized:
            return


if __name__ == '__main__':
    rc = ReactiveController()
    rospy.spin()